/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-async-web-server-espasyncwebserver-library/
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

// Import required libraries
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ESP32Servo.h>

// Replace with your network credentials
const char* ssid = "YOUR NETWORK";
const char* password = "YOUR NETWORK PASSWORD";

const char* PARAM_INPUT_1 = "output";
const char* PARAM_INPUT_2 = "state";

int pan_pin = 23;
int tilt_pin = 22;

int pan = 90;
int tilt = 90;

Servo g_pan_servo;
Servo g_tilt_servo;

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html> <html>
<head><meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<title>Andy's Pan/Tilt Control</title>
<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}
body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}
.button {width: 80px;background-color: #3498db;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}
.button-on {background-color: #3498db;}
.button-on:active {background-color: #2980b9;}
.button-off {background-color: #34495e;}
.button-off:active {background-color: #2c3e50;}
p {font-size: 14px;color: #888;margin-bottom: 10px;}

/* Mouse-over effects */
.slider:hover {
  opacity: 1; /* Fully shown on mouse-over */
}


.slider {
  -webkit-appearance: none;
  width: 50%%;
  height: 15px;
  border-radius: 5px;  
  background: #d3d3d3;
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  border-radius: 50%%; 
  background: #4CAF50;
  cursor: pointer;
}

.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  border-radius: 50%%;
  background: #4CAF50;
  cursor: pointer;
}
</style>
</head>
<body>
<h1>Andy's Pan / Tilt Control</h1>
<h3>(also can use arrow keys)</h3>
<p>Pan</p>
<div class="slidecontainer">
  <input type="range" min="0" max="180" value="90" class="slider" id="pan_slider">
  <p id="pan_value">90</p>
</div>
<br />
<a class="button" onclick="change_pan(-10);">&#8592;</a>
<a class="button" onclick="change_pan(10);">&#8594;</a>



<br /><br />

<p>Tilt</p>
<div class="slidecontainer">
  <input type="range" min="0" max="180" value="90" class="slider" id="tilt_slider">
  <p id="tilt_value">90</p>
</div>
<br />
<a class="button button-off" onclick="change_tilt(-10);">&#8595;</a>
<a class="button button-off" onclick="change_tilt(10);">&#8593;</a>
<br /><br />

<script>

function callback(pan_tilt, value) {
    console.log("callback:")
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            //document.getElementById("demo").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/update?output=" + pan_tilt + "&state=" + value, true);
    xhttp.send();
}
    
document.onkeyup = function(e) {
  console.log(e.which)
  if (e.which == 87 || e.which == 38) {
    // w
    change_tilt(10)
  } else if (e.which == 65 || e.which == 37) {
    // a
    change_pan(-10)
  } else if (e.which == 83 || e.which == 40) {
      // s
      change_tilt(-10)
  } else if (e.which == 68 || e.which == 39) {
      // d
      change_pan(10)
  }
};

function change_pan(delta) {
    pan_slider.value = parseInt(pan_slider.value) + delta
    pan_output.innerHTML = '<b>' + pan_slider.value + '</b>'
    callback('pan', pan_slider.value)
}

function change_tilt(delta) {
    tilt_slider.value = parseInt(tilt_slider.value) + delta
    tilt_output.innerHTML = '<b>' + tilt_slider.value + '</b>'
    callback('tilt', tilt_slider.value)
}

var pan_slider = document.getElementById("pan_slider");
var pan_output = document.getElementById("pan_value");
pan_output.innerHTML = pan_slider.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
pan_slider.oninput = function() {
  pan_output.innerHTML = this.value;
  callback('pan', this.value);
}


var tilt_slider = document.getElementById("tilt_slider");
var tilt_output = document.getElementById("tilt_value");
tilt_output.innerHTML = tilt_slider.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
tilt_slider.oninput = function() {
  tilt_output.innerHTML = this.value;
  callback('tilt', this.value);
}

</script>

</body>
</html>

)rawliteral";

// Replaces placeholder with button section in your web page
String processor(const String& var){
  //Serial.println(var);
  if(var == "BUTTONPLACEHOLDER"){
    String buttons = "";
    buttons += "<h4>Output - GPIO 2</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"slider\"></span></label>";
    buttons += "<h4>Output - GPIO 4</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"4\" " + outputState(4) + "><span class=\"slider\"></span></label>";
    buttons += "<h4>Output - GPIO 33</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"33\" " + outputState(33) + "><span class=\"slider\"></span></label>";
    return buttons;
  }
  return String();
}

String outputState(int output){
  if(digitalRead(output)){
    return "checked";
  }
  else {
    return "";
  }
}

void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);

  g_pan_servo.attach(pan_pin);
  g_tilt_servo.attach(tilt_pin);

  update_servos();

  pinMode(2, OUTPUT);
  digitalWrite(2, LOW);
  pinMode(4, OUTPUT);
  digitalWrite(4, LOW);
  pinMode(33, OUTPUT);
  digitalWrite(33, LOW);
  
  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
  server.on("/update", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage1;
    String inputMessage2;
    // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
    if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2)) {
      inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
      inputMessage2 = request->getParam(PARAM_INPUT_2)->value();

      if (inputMessage1 == "pan") {
        pan = inputMessage2.toInt();
        update_servos();
      } else if (inputMessage1 == "tilt") {
        tilt = 180 - inputMessage2.toInt();
        update_servos();
      }
      
      Serial.println(inputMessage1);
      Serial.println(inputMessage2);
    }
    else {
      inputMessage1 = "No message sent";
      inputMessage2 = "No message sent";
    }
    Serial.print("GPIO: ");
    Serial.print(inputMessage1);
    Serial.print(" - Set to: ");
    Serial.println(inputMessage2);
    request->send(200, "text/plain", "OK");
  });

  // Start server
  server.begin();
}

void loop() {

}

void update_servos() {
  Serial.print("Pan: ");
  Serial.print(pan);
  Serial.print(" , Tilt: ");
  Serial.println(tilt);
  g_pan_servo.write(pan);
  g_tilt_servo.write(tilt);
}
